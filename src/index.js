require('./models/User');
require('./models/Track');
const express = require('express');
const mongoose = require('mongoose');
const bodyParse = require('body-parser')
const authRoutes = require('./routes/authRoutes');
const trackRoutes = require('./routes/trackRoutes')
const requireAuth = require('../src/middleware/requireAuth');

const app = express();
app.use(bodyParse.json());
app.use(authRoutes);
app.use(trackRoutes);

const mongoURI = 'mongodb+srv://root:passwordpassword@cluster0-mqlv1.mongodb.net/test?retryWrites=true&w=majority';

mongoose.connect(mongoURI, {
    useNewUrlParser : true,
    useCreateIndex: true
});

mongoose.connection.on('connected', () => {
    console.log('Connected to mongo instance');
});

mongoose.connection.on('error', (err) => {
    console.log('Error connection to mongo db: ', err);
});

app.get('/', requireAuth, (req,res) => {
    res.send(`Your Email: ${req.user.email}`);
});

app.listen(3000, () => {
    console.log('Listening to Port 3000');
});